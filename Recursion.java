public class Recursion{
  public static void main(String[] args){
    
  }
  //with recursion
  public int recursiveCount(int[] numbers, int n){
    if(numbers.length < n++){
      return 0;
    }
    if(numbers[n] < 10 && n%2==0){
      return recursiveCount(numbers, n++) + 1;
    }
    else{
      return recursiveCount(numbers, n++); 
    }
  }
  //without recursion (breaking down problem)
  /*public int recursiveCount(int[] numbers, int n){
    int index = 0;
    int count = 0;
    for(int x : numbers){
      if(x < 10){
        count++;
      }
      if(index%2==0 && numbers[index] >= n){
        count++;
      }*/
}