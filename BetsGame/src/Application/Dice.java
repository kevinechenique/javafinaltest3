package Application;

import java.util.Random;

public class Dice {
	private int diceValue;
	
	
	//returns a value of the dice from 1 to 6
	public int rollDice() {
		Random rand = new Random();
		int[] diceValues = {1,2,3,4,5,6};
		this.diceValue = diceValues[rand.nextInt(6)];
		return this.diceValue;
	}
	

}
