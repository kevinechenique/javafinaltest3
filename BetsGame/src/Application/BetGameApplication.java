package Application;

public class BetGameApplication {
	
	private int money;
	private int betQuantity;
	private boolean isLarge;//gives true when the roll is large
	private String playerChoice;//whether the player chose "large" or "small"
	private boolean wonRound;//true when player wins the round
	private Dice dice;
	
	public BetGameApplication(int betQuantity, String playerChoice) {
		this.betQuantity = betQuantity;
		this.money = 500;
		this.isLarge = false;
		this.playerChoice = playerChoice;
	}
	
	//This method will set the private field isLarge that determines if the value is a large or a small
	public void largeOrNot(Dice dice) {
		//if player got a small value
		if(dice.rollDice()<=3) {
			this.isLarge = false;
		}
		else if(dice.rollDice()>=4) {
			this.isLarge = true;
		}
	}
	//This method will determine if the player won or not and display a message
	public String winOrLoseMessage() {
		if(this.isLarge && this.playerChoice.equals("large")) {
			return "You bet large and you won";
		}
		else if(this.isLarge && this.playerChoice.equals("small")) {
			return "You bet small and you lost";
		}
		else if(!(this.isLarge) && this.playerChoice.equals("small")) {
			return "You bet small and you won";
		}
		else if(!(this.isLarge) && this.playerChoice.equals("large")) {
			return "you bet large and you lost";
		}
		return " ";
	}
	//this method will return true if player won the round
	public boolean wonMatch() {
			if(this.isLarge && this.playerChoice.equals("large")) {
				return this.wonRound = true;
			}
			else if(this.isLarge && this.playerChoice.equals("small")) {
				return this.wonRound = false;
			}
			else if(!(this.isLarge) && this.playerChoice.equals("small")) {
				return this.wonRound = true;
			}
			else if(!(this.isLarge) && this.playerChoice.equals("large")) {
				return this.wonRound = false;
			}
			return false;
	};
	
	//This method will calculate the total money that a player has
	public int totalGains() {
		int money = 0;//add or subtracts depending whether the player won or lost
		if(wonMatch()) {
			money += this.betQuantity + this.betQuantity;
			this.money += money;
		}
		if(!(wonMatch())) {
			money -= this.betQuantity;
			this.money += money;
		}
		return this.money;
		
	}

}
