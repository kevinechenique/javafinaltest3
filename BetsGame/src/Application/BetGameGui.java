package Application;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class BetGameGui extends Application{

	@Override
	public void start(Stage stage) throws Exception {
		
		Group root = new Group();
		
		Scene scene = new Scene(root, 650, 300);
		scene.setFill(Color.BLACK);
		
		//buttons and layout
		HBox buttons = new HBox();//to layout buttons
		HBox textFields = new HBox();
		VBox vb = new VBox();
		
		//buttons and textFields
		Button large = new Button("large");
		Button small = new Button("small");
		TextField betQuantity = new TextField("Quantity to bet");
		TextField moneyLeft = new TextField("Money left: ");
		
		//BetGameApplication game = new BetGameApplication();
		
		buttons.getChildren().addAll(large, small);
		textFields.getChildren().addAll(betQuantity, moneyLeft);
		
		vb.getChildren().addAll(buttons, textFields);
		
		stage.setTitle("Rolling Dice Game");
		stage.setScene(scene);
		
		stage.show();
	}
	
	public static void main(String[] args) {
		Application.launch(args);
	}
	
}
